# Cinco macOS Installer
A `bash` script for building macOS installer packages (`.pkg`) and packaging them in disk images (`.dmg`).

## Prerequisites
- macOS 10.14 "Mojave" (or later) or
- Ubuntu 18.04 "Bionic Beaver"

## Setup
```bash
$ sudo ./build-installer.sh --install
```
- Installation requires root privileges (`sudo`).
- Installs the script to `/usr/local/bin/build-installer` (macOS) or `/usr/bin/build-installer` (Ubuntu).
- On Ubuntu: Installs dependencies `mkbom`, `xar` and `xml2`, if they are missing.

## How to create a macOS installer
1. Compile a Cinco app bundle.
2. Put any additional [resources](#resources) for the installer in a folder. (See `resources-example` and `resources-cinco` for examples.)
3. Run the `build-installer` script.
4. Publish the generated DMG file.

## Help / Usage Information
```
build-installer --app <path> [--name <string>] [--resources <path>] [--output <path>] [--yes]
build-installer --install [--yes]
build-installer --help

--app <path>
-a <path>
    Required.
    Path to the app bundle.

--name <string>
-n <string>
    Optional.
    Target name of the app bundle (excl. ".app" extension).
    If omitted, the script will derive the app's name from its "Info.plist".
    Only use this option if you want to create an alternative version of the app
    (e.g. for nightly builds).

--resources <path>
-r <path>
    Optional.
    Path to a folder containing additional resources for the installer.
    If omitted, the current working directory will be used.
    The folder may contain:
    - "icon.icns"
    - "background.png" or ("background-light.png" and "background-dark.png")
    - "welcome.rtf"    or "welcome.txt"
    - "readme.rtf"     or "readme.txt"
    - "license.rtf"    or "license.txt"
    - "conclusion.rtf" or "conclusion.txt"

--output <path>
-o <path>
    Optional.
    Path to the desired output file.
    If omitted, the DMG file will be saved in the current working directory with
    a default name.

--yes
-y
    Optional.
    If used, all decisions are accepted, that are usually prompted during the
    execution of this script.

--install
-i
    Installs the script to "/usr/local/bin/build-installer" on macOS or
    "/usr/bin/build-installer" on Ubuntu.
    Also installs missing dependencies for Ubuntu.
    Requires root privileges (sudo).

--help
-h
    Prints this message and exits.
```

## Resources
- macOS icon
    - Add a `icon.icns` to your resources folder.
    - This icon will replace the the app's icon.
    - Useful, if the app bundle's icon is missing or if you want to create an
      alternative version of the app (e.g. for nightly builds).
- Background image
	- Add a `background.png` to your resources folder.
	- If you want different appearances for light and dark mode, use `background-light.png` and `background-dark.png` instead.
	- Background images should be 1240 x 832 pixels.
	- Background images can be transparent.
- Welcome message
	- Add a `welcome.rtf` or `welcome.txt` to your resources folder.
	- The welcome message is the first thing the user will see.
	- A default message will be displayed, if no welcome message is provided.
- ReadMe
	- Add a `readme.rtf` or `readme.txt` to your resources folder.
	- The ReadMe will be displayed after the welcome message.
	- This step in the installation process will be skipped, if no ReadMe is provided.
- License
	- Add a `license.rtf` or `license.txt` to your resources folder.
	- The license will be displayed after the ReadMe.
	- The user will be asked, if they agree to the license terms. The installation does not start before the user agrees.
	- This step in the installation process will be skipped, if no license is provided.
- Conclusion
	- Add a `conclusion.rtf` or `conclusion.txt` to your resources folder.
	- The conclusion will be displayed after a successful installation.
	- A default message will be displayed, if no conclusion is provided.

## Infos
- macOS
	- [How to use `pkgbuild` and `productbuild`](https://stackoverflow.com/questions/48077574/signing-and-archiving-mac-app-with-productbuild-without-using-xcode-for-distribu)
	- [Distribution file documentation](https://developer.apple.com/library/archive/documentation/DeveloperTools/Reference/DistributionDefinitionRef/Chapters/Distribution_XML_Ref.html)
	- [How to create a DMG file](https://www.recitalsoftware.com/blogs/148-howto-build-a-dmg-file-from-the-command-line-on-mac-os-x)
- Linux
	- [How to use `bomutils`](http://bomutils.dyndns.org/tutorial.html)
	- [Script for building installers](https://medium.com/@SchizoDuckie/how-to-build-an-osx-pkg-installer-for-your-node-webkit-app-on-linux-b53a13c4ad55)
