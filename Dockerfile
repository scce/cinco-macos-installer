FROM ubuntu:18.04

RUN apt-get update && apt-get install -y --no-install-recommends \
                        cpio \
                        mkisofs \
                        unzip \
                        wget

WORKDIR /installer

COPY resources-cinco resources-cinco
COPY build-installer.sh .

RUN ./build-installer.sh --install --yes

ENTRYPOINT ./build-installer.sh
CMD ['--help']
