#!/bin/bash

USAGE="
$0 --app <path> [--name <string>] [--resources <path>] [--output <path>] [--yes]
$0 --install [--yes]
$0 --help

--app <path>
-a <path>
    Required.
    Path to the app bundle.

--name <string>
-n <string>
    Optional.
    Target name of the app bundle (excl. \".app\" extension).
    If omitted, the script will derive the app's name from its \"Info.plist\".
    Only use this option if you want to create an alternative version of the app
    (e.g. for nightly builds).

--resources <path>
-r <path>
    Optional.
    Path to a folder containing additional resources for the installer.
    If omitted, the current working directory will be used.
    The folder may contain:
    - \"icon.icns\"
    - \"background.png\" or (\"background-light.png\" and \"background-dark.png\")
    - \"welcome.rtf\"    or \"welcome.txt\"
    - \"readme.rtf\"     or \"readme.txt\"
    - \"license.rtf\"    or \"license.txt\"
    - \"conclusion.rtf\" or \"conclusion.txt\"

--output <path>
-o <path>
    Optional.
    Path to the desired output file.
    If omitted, the DMG file will be saved in the current working directory with
    a default name.

--yes
-y
    Optional.
    If used, all decisions are accepted, that are usually prompted during the
    execution of this script.

--install
-i
    Installs the script to \"/usr/local/bin/build-installer\" on macOS or
    \"/usr/bin/build-installer\" on Ubuntu.
    Also installs missing dependencies for Ubuntu.
    Requires root privileges (sudo).

--help
-h
    Prints this message and exits.
"

################################################################################
# Build scripts                                                                #
################################################################################

# The main script (* arguments)
build() {

	# Exit script if any errors occur
	set -e

	# Formatting
	NORMAL="\033[0m"
	BOLD="\033[0;1m"
	NORMAL_GREEN="\033[0;32m"
	BOLD_GREEN="\033[1;32m"
	NORMAL_YELLOW="\033[0;33m"
	BOLD_YELLOW="\033[1;33m"
	NORMAL_RED="\033[0;31m"
	BOLD_RED="\033[1;31m"

	# Global variables (All of those will be set automatically)
	OS_KERNEL=""            # Name of the Kernel of the current OS (e.g. "Darwin", "Linux")
	OS_NAME=""              # Name of the OS/Distribution (e.g. "macOS", "Ubuntu")
	OS_VERSION=""           # Version of the OS (e.g. "11.2.0", "18.04")
	WD_PATH=""              # Absolute path to the working directory
	APP_PATH=""             # Absolute path to the app bundle (e.g. "/path/to/cinco-1.2.app")
	APP_NAME=""             # Name of the app (e.g. "Cinco")
	APP_VERSION=""          # Version of the app (e.g. "1.2.0")
	APP_VERSION_BUILD=""    # Longer app version (e.g. "1.2.0.202102150306")
	APP_IDENTIFIER=""       # macOS bundle identifier of the app (e.g. "de.jabc.cinco.meta.product.product")
	APP_ICON=""             # File name of the app's icon (e.g. "Eclipse.icns")
	APP_NAME_FLAT=""        # A flattened version of the app's name (e.g. "Cinco Pro 2.0" -> "cinco-pro-2-0")
	APP_VERSION_FLAT=""     # A flattened version of the app's version (incl. build) (e.g.
	                        # ["1.2.0" "Build 123DEF"] -> "1.2.0-build-123def",
	                        # ["1.2.0" "1.2.0.202102150306"] -> "1.2.0.202102150306")
	RESOURCES_PATH=""       # Absolute path to the resource folder (e.g. "/path/to/resources")
	OUTPUT_PATH=""          # Absolute or relative path to the desired output file
	                        # (e.g. "target/cinco-1.2.0-202102150306.dmg")
	TMP_PATH=""             # Absolute path to the temporary folder (e.g. "/tmp/build-cinco-installer")
	ACCEPT_ALL=""           # Whether or not to accept all decisions that are usually prompted
	INSTALL=""              # Whether or not to install the script
	MISSING_DEPENDENCIES="" # Whether any Linux dependencies are missing or not
	ABSOLUTE_PATH_RESULT="" # Result of the absolutePath() function
	URL_ENCODE_RESULT=""    # Result of the urlEncode() function

	# Parse script's arguments
	parseArguments "$@"

	# Check if root user before install
	if [[ -n $INSTALL && $(id -u) != 0 ]]; then
		printError "This script requires root privileges (sudo) for installation."
	fi

	# Save current working directory
	WD_PATH=$(pwd)

	# Check OS
	checkOS

	# Install script, if "--install" option was set
	if [[ -n $INSTALL ]]; then
		installScript
		installLinuxDependencies
		exit 0
	fi

	# Check Linux dependencies
	checkLinuxDependencies
	if [[ -n $MISSING_DEPENDENCIES ]]; then
		printError "Some dependencies are missing. Use \"--install\" to install them."
	fi

	# Handle script arguments
	printInfo "Preparing variables ..."
	setAppPath "${APP_PATH}"
	setResourcesPath "${RESOURCES_PATH}"
	setOutputPath "${OUTPUT_PATH}"

	echo "App path: ${APP_PATH}"
	echo "App name: ${APP_NAME}"
	echo "App version: ${APP_VERSION} (${APP_VERSION_BUILD})"
	echo "App identifier: ${APP_IDENTIFIER}"
	echo "App icon: ${APP_ICON}"
	echo "Resources path: ${RESOURCES_PATH}"
	echo "Output path: ${OUTPUT_PATH}"

	# Create temporary folder
	printInfo "Creating temporary folder ..."
	TMP_PATH="/tmp/build-${APP_NAME_FLAT}-installer"
	echo "Temporary folder: \"${TMP_PATH}\""
	if [[ -d "${TMP_PATH}" ]]; then
		rm -r "${TMP_PATH}"
	fi
	mkdir -p "${TMP_PATH}/volume"
	mkdir -p "${TMP_PATH}/root/Applications"
	mkdir -p "${TMP_PATH}/package"
	mkdir -p "${TMP_PATH}/package/Resources"

	# Copy app bundle
	printInfo "Copying app bundle ..."
	echo "Copying \"${APP_PATH}\" -> \"${TMP_PATH}/root/Applications/${APP_NAME}.app\""
	cp -R "${APP_PATH}" "${TMP_PATH}/root/Applications/${APP_NAME}.app"

	# Copy resources
	printInfo "Copying resources ..."
	copyResources

	# Run corresponding script
	if [[ $OS_KERNEL == "Darwin" ]]; then
		buildOnMacOS
	else
		buildOnLinux
	fi

	# Remove temporary folder
	printInfo "Removing temporary folder ..."
	echo "Removing \"${TMP_PATH}\""
	rm -r "${TMP_PATH}"

	# Show result
	printInfo "Done" "\"${OUTPUT_PATH}\""

}

# Building procedure for macOS (0 arguments)
buildOnMacOS() {

	# Build package component
	printInfo "Building package component ..."
	buildComponentPlist
	pkgbuild --root "${TMP_PATH}/root" --component-plist "${TMP_PATH}/package/Component.plist" --identifier "${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component" --version "${APP_VERSION}" "${TMP_PATH}/package/${APP_NAME}.pkg"

	# Build "Distribution" file
	printInfo "Building distribution information ..."
	# productbuild --synthesize --package "${TMP_PATH}/package/${APP_NAME}.pkg" "${TMP_PATH}/package/Distribution"
	buildDistribution

	# Build package
	printInfo "Building package ..."
	productbuild --distribution "${TMP_PATH}/package/Distribution" --package-path "${TMP_PATH}/package" --resources "${TMP_PATH}/package/Resources" --identifier "${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.product" --version "${APP_VERSION}" "${TMP_PATH}/volume/Install ${APP_NAME}.pkg"

	# Create writable DMG
	printInfo "Creating DMG ..."
	hdiutil create "${TMP_PATH}/volume.dmg" -ov -volname "${APP_NAME}" -fs "HFS+" -srcfolder "${TMP_PATH}/volume"

	# Convert to compressed read-only DMG
	printInfo "Converting DMG to compressed read-only ..."
	hdiutil convert "${TMP_PATH}/volume.dmg" -ov -format "UDZO" -o "${OUTPUT_PATH}"

}

# Building procedure for Linux (0 arguments)
buildOnLinux() {

	# Create package component folder
	mkdir -p "${TMP_PATH}/package/${APP_NAME}.pkg"

	# Build "PackageInfo" file
	printInfo "Building package information ..."
	buildPackageInfo

	# Build "Distribution" file
	printInfo "Building distribution information ..."
	buildDistribution

	# Build "Payload" file
	printInfo "Building payload ..."
	cd "${TMP_PATH}/root"
	find "." | cpio -o --format "odc" --owner "0:80" | gzip -c > "${TMP_PATH}/package/${APP_NAME}.pkg/Payload"
	cd "${WD_PATH}"

	# Build "Bom" file
	printInfo "Building BOM ..."
	mkbom -u 0 -g 80 "${TMP_PATH}/root" "${TMP_PATH}/package/${APP_NAME}.pkg/Bom"

	# Build package
	printInfo "Building package ..."
	cd "${TMP_PATH}/package"
	xar --compression "none" -cf "${TMP_PATH}/volume/Install ${APP_NAME}.pkg" *
	cd "${WD_PATH}"

	# Create DMG
	printInfo "Creating DMG ..."
	mkisofs -o "${OUTPUT_PATH}" -r -l -ldots -V "${APP_NAME}" "${TMP_PATH}/volume"

}

################################################################################
# Checks                                                                       #
################################################################################

# Checks if using macOS 10.14 "Mojave" (or later)
# or Ubuntu 18.04 "Bionic Beaver" (0 arguments)
checkOS() {

	printInfo "Checking OS ..."
	OS_KERNEL=$(uname -s)
	if [[ $OS_KERNEL == "Darwin" ]]; then
		OS_NAME=$(sw_vers -productName)
		OS_VERSION=$(sw_vers -productVersion)
		echo "OS: ${OS_NAME} ${OS_VERSION}"
		local OS_VERSION_MAJOR=$(echo $OS_VERSION | perl -pe 's|^(\d+)\.(\d)+(\.\d+)?$|$1|')
		local OS_VERSION_MINOR=$(echo $OS_VERSION | perl -pe 's|^(\d+)\.(\d)+(\.\d+)?$|$2|')
		if [[ $OS_VERSION_MAJOR == 10 && $OS_VERSION_MINOR -ge 14 ]]; then
			return
		elif [[ $OS_VERSION_MAJOR -ge 11 ]]; then
			return
		fi
	elif [[ $OS_KERNEL == "Linux" ]]; then
		if [[ -f "/etc/os-release" ]]; then
			OS_NAME=$(cat "/etc/os-release" | sed -nE 's|^NAME="(.+)"$|\1|p')
			OS_VERSION=$(cat "/etc/os-release" | sed -nE 's|^VERSION_ID="(.+)"$|\1|p')
			echo "OS: ${OS_NAME} ${OS_VERSION}"
			if [[ $OS_NAME == "Ubuntu" && $OS_VERSION == "18.04" ]]; then
				return
			fi
		else
			echo "OS: Unknown Linux"
		fi
	fi

	while true; do
		printWarning "This script was build for macOS 10.14 \"Mojave\" (or later)\nand Ubuntu 18.04 \"Bionic Beaver\"."
		if [[ -n $ACCEPT_ALL ]]; then
			echo "Trying to proceed automatically ..."
			return
		else
			echo "Do you want to continue anyway? [yes/no]"
			read -p "> " READ_RESPONSE
			if [[ $READ_RESPONSE == "n" || $READ_RESPONSE == "no" ]]; then
				echo "Okay. Bye!"
				exit 0
			elif [[ $READ_RESPONSE == "y" || $READ_RESPONSE == "yes" ]]; then
				return
			fi
		fi
	done

}

# Checks if all necessary executables are available and writes the result to
# $MISSING_DEPENDENCIES (0 arguments)
checkLinuxDependencies() {

	MISSING_DEPENDENCIES=""

	if [[ $OS_KERNEL == "Linux" ]]; then

		printInfo "Checking Linux dependencies ..."
		local DEPENDENCIES="mkbom xar xml2"
		for DEPENDENCY in $DEPENDENCIES; do
		    if [[ -n $(command -v $DEPENDENCY) ]]; then
		    	echo "${DEPENDENCY} ... OK"
		    else
		    	echo "${DEPENDENCY} ... Missing"
		    	MISSING_DEPENDENCIES="yes"
		    fi
		done

	fi

}

################################################################################
# Installers                                                                   #
################################################################################

# Installs this script (0 arguments)
installScript() {

	printInfo "Installing script ..."

	# Set install destination
	if [[ $OS_KERNEL == "Darwin" ]]; then
		local INSTALL_PATH="/usr/local/bin/build-installer"
	else
		local INSTALL_PATH="/usr/bin/build-installer"
	fi

	# Check if already installed
	if [[ -f $INSTALL_PATH ]]; then
		if [[ $0 == $INSTALL_PATH ]]; then
			echo "Already installed in \"${INSTALL_PATH}\"."
			printInfo "Done" "Skipped installing script."
			return
		fi
		while true; do
			printWarning "Another version is already installed in \"${INSTALL_PATH}\"."
			if [[ -n $ACCEPT_ALL ]]; then
				echo "Reinstalling automatically ..."
				break
			else
				echo "Do you want to reinstall? [yes/no]"
				read -p "> " READ_RESPONSE
				if [[ $READ_RESPONSE == "n" || $READ_RESPONSE == "no" ]]; then
					printInfo "Done" "Skipped installing script."
					return
				elif [[ $READ_RESPONSE == "y" || $READ_RESPONSE == "yes" ]]; then
					break
				fi
			fi
		done
	fi

	# Install
	echo "Copying \"$0\" -> \"${INSTALL_PATH}\""
	cp "$0" "${INSTALL_PATH}"
	chmod +x "${INSTALL_PATH}"

	# Show result
	printInfo "Done" "Installed script to \"${INSTALL_PATH}\""

}

# Installs all necessary executables for Linux (0 arguments)
installLinuxDependencies() {

	checkLinuxDependencies

	if [[ $OS_KERNEL == "Linux" && -n $MISSING_DEPENDENCIES ]]; then

		printInfo "Installing Linux dependencies ..."

		# Install git, g++, make, libxml2-dev, xml2, libssl1.0-dev
		printInfo "Installing git, g++, make, libxml2-dev, xml2, libssl1.0-dev ..."
		apt update
		apt install git g++ make libxml2-dev xml2 libssl1.0-dev -y

		# Create temporary folder
		printInfo "Creating temporary folder ..."
		TMP_PATH="/tmp/build-installer-linux-dependencies"
		if [[ -d $TMP_PATH ]]; then
			rm -rf "${TMP_PATH}"
		fi
		mkdir -p "${TMP_PATH}"
		echo "Temporary folder: \"/${TMP_PATH}\""

		# Install bomutils
		printInfo "Installing bomutils"
		cd "${TMP_PATH}"
		git clone "https://github.com/hogliux/bomutils.git" "bomutils"
		cd "bomutils"
		make
		make install

		# Install xar
		printInfo "Installing xar"
		cd "${TMP_PATH}"
		wget "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/xar/xar-1.5.2.tar.gz"
		tar -zxf "xar-1.5.2.tar.gz"
		cd "xar-1.5.2"
		./configure
		make
		make install

		# Remove temporary folder
		printInfo "Removing temporary folder ..."
		cd "${WD_PATH}"
		echo "Removing \"${TMP_PATH}\""
		rm -rf "${TMP_PATH}"

		# Show result
		printInfo "Done" "Installed Linux dependencies"

	fi

}

################################################################################
# Variable setters                                                             #
################################################################################

# Sets variables according to script arguments (* arguments)
parseArguments() {

	if [[ $# == 0 ]]; then
		printUsage
	fi

	while [[ $# != 0 ]]; do
		case $1 in
			-a|--app|--application)
				APP_PATH=$2
				shift; shift;;
			-n|--name)
				APP_NAME=$2
				shift; shift;;
			-r|--resources)
				RESOURCES_PATH=$2
				shift; shift;;
			-o|--output)
				OUTPUT_PATH=$2
				shift; shift;;
			-y|--yes)
				ACCEPT_ALL="yes"
				shift;;
			-i|--install)
				INSTALL="yes"
				shift;;
			-h|-help|--help)
				printUsage;;
			*)
				printError "Invalid arguments. Use \"--help\" for usage information.";;
		esac
	done

	if [[ (-z $APP_PATH && -z $INSTALL) || (-n $APP_PATH && -n $INSTALL) ]]; then
		printError "Invalid arguments. Use \"--help\" for usage information."
	fi

}

# Sets all APP_* variables given a path to an app (1 argument)
setAppPath() {

	# Check argument
	if [[ $# != 1 || -z $1 ]]; then
		printError "setAppPath(): Expected 1 argument, but got $#."
	fi
	if [[ ! -d $1 ]]; then
		printError "setAppPath(): The app \"$1\" does not exist or is not an app bundle."
	fi
	if [[ ! $1 =~ \.app/?$ ]]; then
		printError "setAppPath(): The app \"$1\" is not an app bundle."
	fi

	# Set $APP_PATH
	absolutePath "$1"
	APP_PATH=$ABSOLUTE_PATH_RESULT
	if [[ ! -f "${ABSOLUTE_PATH_RESULT}/Contents/Info.plist" ]]; then
		printError "setAppPath(): The app \"$@\" is missing its Info.plist."
	fi

	# Set other APP_* variables
	if [[ $OS_KERNEL == "Darwin" ]]; then
		if [[ -z $APP_NAME ]]; then
			APP_NAME=$(defaults read "${APP_PATH}/Contents/Info.plist" "CFBundleName")
		fi
		APP_VERSION=$(defaults read "${APP_PATH}/Contents/Info.plist" "CFBundleShortVersionString")
		APP_VERSION_BUILD=$(defaults read "${APP_PATH}/Contents/Info.plist" "CFBundleVersion")
		APP_IDENTIFIER=$(defaults read "${APP_PATH}/Contents/Info.plist" "CFBundleIdentifier")
		APP_ICON=$(defaults read "${APP_PATH}/Contents/Info.plist" "CFBundleIconFile")
	else # Linux
		local INFO_PLIST=$(xml2 < "${ABSOLUTE_PATH_RESULT}/Contents/Info.plist")
		if [[ -z $APP_NAME ]]; then
			APP_NAME=$(echo $INFO_PLIST | sed -nE 's|.*/plist/dict/key=CFBundleName\s+/plist/dict/string=(\S+).*|\1|p')
		fi
		APP_VERSION=$(echo $INFO_PLIST | sed -nE 's|.*/plist/dict/key=CFBundleShortVersionString\s+/plist/dict/string=(\S+).*|\1|p')
		APP_VERSION_BUILD=$(echo $INFO_PLIST | sed -nE 's|.*/plist/dict/key=CFBundleVersion\s+/plist/dict/string=(\S+).*|\1|p')
		APP_IDENTIFIER=$(echo $INFO_PLIST | sed -nE 's|.*/plist/dict/key=CFBundleIdentifier\s+/plist/dict/string=(\S+).*|\1|p')
		APP_ICON=$(echo $INFO_PLIST | sed -nE 's|.*/plist/dict/key=CFBundleIconFile\s+/plist/dict/string=(\S+).*|\1|p')
	fi

	# Set APP_*_FLAT variables
	APP_NAME_FLAT=$(echo $APP_NAME | tr "[:upper:]" "[:lower:]" | tr "[:blank:]" "-" | tr "[:punct:]" "-" | tr -s "-")
	APP_VERSION_FLAT=$(echo $APP_VERSION | tr "[:upper:]" "[:lower:]" | tr "[:blank:]" "-" | tr -s "-")
	local APP_VERSION_BUILD_FLAT=$(echo $APP_VERSION_BUILD | tr "[:upper:]" "[:lower:]" | tr "[:blank:]" "-" | tr -s "-")
	if [[ $APP_VERSION_FLAT == *"${APP_VERSION_BUILD_FLAT}"* ]]; then
		APP_VERSION_FLAT=$APP_VERSION_FLAT
	elif [[ $APP_VERSION_BUILD_FLAT == *"${APP_VERSION_FLAT}"* ]]; then
		APP_VERSION_FLAT=$APP_VERSION_BUILD_FLAT
	else
		APP_VERSION_FLAT="${APP_VERSION_FLAT}-${APP_VERSION_BUILD_FLAT}"
	fi

}

# Sets the RESOURCES_PATH variable (0..1 arguments)
# (Uses default value, if no argument is provided)
setResourcesPath() {

	if [[ $# == 0 || -z $1 ]]; then
		absolutePath "."
	elif [[ $# == 1 ]]; then
		if [[ ! -d $1 ]]; then
			printError "setResourcesPath(): The resources folder \"$1\" does not exist."
		fi
		absolutePath "$1"
	else
		printError "setResourcesPath(): Expected 1 argument, but got $#."
	fi
	RESOURCES_PATH=$ABSOLUTE_PATH_RESULT

}

# Sets the OUTPUT_PATH variable (0..1 arguments)
# (Uses default value, if no argument is provided)
setOutputPath() {

	if [[ $# == 0 || -z $1 ]]; then
		if [[ -n $APP_NAME_FLAT && -n $APP_VERSION_FLAT ]]; then
			OUTPUT_PATH="${APP_NAME_FLAT}-${APP_VERSION_FLAT}-macos.dmg"
		elif [[ -n $APP_NAME_FLAT ]]; then
			OUTPUT_PATH="${APP_NAME_FLAT}-macos.dmg"
		else
			OUTPUT_PATH="installer-macos.dmg"
		fi
	elif [[ $# == 1 ]]; then
		if [[ $1 =~ \.dmg$ ]]; then
			OUTPUT_PATH="$1"
		else
			printError "setOutputPath(): The extension of the output file must be \".dmg\"."
		fi
	else
		printError "setOutputPath(): Expected 1 argument, but got $#."
	fi

}

################################################################################
# Utilities                                                                    #
################################################################################

# Puts the absolute path of the input in $ABSOLUTE_PATH_RESULT (1 argument)
# (The input path must be an existing file or directory)
absolutePath() {

	if [[ $# != 1 || -z $1 ]]; then
		printError "absolutePath(): Expected 1 argument, but got $#."
	fi
	if [[ -d $1 ]]; then
		cd "$1"
		ABSOLUTE_PATH_RESULT=$(pwd -P)
	elif [[ -f $1 ]]; then
		cd $(dirname "$1")
		if [[ $OS_KERNEL == "Darwin" ]]; then
			local FILE_NAME=$(echo $1 | perl -pe 's|.*?([^/]+)$|$1|')
		else
			local FILE_NAME=$(echo $1 | sed -nE 's|.*?([^/]+)$|\1|p')
		fi
		ABSOLUTE_PATH_RESULT="$(pwd -P)/${FILE_NAME}"
	else
		printError "absolutePath(): \"$1\" neither a directory nor a file."
	fi
	cd "${WD_PATH}"

}

# Copies all available resources (0 arguments)
copyResources() {

	# Icon
	if [[ -f "${RESOURCES_PATH}/icon.icns" ]]; then
		mkdir -p "${TMP_PATH}/root/Applications/${APP_NAME}.app/Contents/Resources"
		cp -fv "${RESOURCES_PATH}/icon.icns" "${TMP_PATH}/root/Applications/${APP_NAME}.app/Contents/Resources/${APP_ICON}"
	fi

	# Background
	if [[ -f "${RESOURCES_PATH}/background.png" ]]; then
		cp -v "${RESOURCES_PATH}/background.png" "${TMP_PATH}/package/Resources"
	elif [[ -f "${RESOURCES_PATH}/background-light.png" && -f "${RESOURCES_PATH}/background-dark.png" ]]; then
		cp -v "${RESOURCES_PATH}/background-light.png" "${TMP_PATH}/package/Resources"
		cp -v "${RESOURCES_PATH}/background-dark.png" "${TMP_PATH}/package/Resources"
	fi

	# Welcome
	if [[ -f "${RESOURCES_PATH}/welcome.rtf" ]]; then
		cp -v "${RESOURCES_PATH}/welcome.rtf" "${TMP_PATH}/package/Resources"
	elif [[ -f "${RESOURCES_PATH}/welcome.txt" ]]; then
		cp -v "${RESOURCES_PATH}/welcome.txt" "${TMP_PATH}/package/Resources"
	fi

	# Read me
	if [[ -f "${RESOURCES_PATH}/readme.rtf" ]]; then
		cp -v "${RESOURCES_PATH}/readme.rtf" "${TMP_PATH}/package/Resources"
	elif [[ -f "${RESOURCES_PATH}/readme.txt" ]]; then
		cp -v "${RESOURCES_PATH}/readme.txt" "${TMP_PATH}/package/Resources"
	fi

	# License
	if [[ -f "${RESOURCES_PATH}/license.rtf" ]]; then
		cp -v "${RESOURCES_PATH}/license.rtf" "${TMP_PATH}/package/Resources"
	elif [[ -f "${RESOURCES_PATH}/license.txt" ]]; then
		cp -v "${RESOURCES_PATH}/license.txt" "${TMP_PATH}/package/Resources"
	fi

	# Conclusion
	if [[ -f "${RESOURCES_PATH}/conclusion.rtf" ]]; then
		cp -v "${RESOURCES_PATH}/conclusion.rtf" "${TMP_PATH}/package/Resources"
	elif [[ -f "${RESOURCES_PATH}/conclusion.txt" ]]; then
		cp -v "${RESOURCES_PATH}/conclusion.txt" "${TMP_PATH}/package/Resources"
	fi

}

# Encodes the input and saves the result in $URL_ENCODE_RESULT (1 argument)
urlEncode() {

	URL_ENCODE_RESULT=""
	if [[ $# != 1 || -z $1 ]]; then
		printError "urlEncode(): Expected 1 argument, but got $#."
	fi
	URL_ENCODE_RESULT=$(echo $1 | sed -e 's:%:%25:g' -e 's: :%20:g' -e 's:<:%3C:g' -e 's:>:%3E:g' -e 's:#:%23:g' -e 's:{:%7B:g' -e 's:}:%7D:g' -e 's:|:%7C:g' -e 's:\\:%5C:g' -e 's:\^:%5E:g' -e 's:~:%7E:g' -e 's:\[:%5B:g' -e 's:\]:%5D:g' -e 's:`:%60:g' -e 's:;:%3B:g' -e 's:/:%2F:g' -e 's:?:%3F:g' -e 's^:^%3A^g' -e 's:@:%40:g' -e 's:=:%3D:g' -e 's:&:%26:g' -e 's:\$:%24:g' -e 's:\!:%21:g' -e 's:\*:%2A:g')

}

################################################################################
# Write files                                                                  #
################################################################################

# Builds a Component.plist file (0 arguments)
buildComponentPlist() {

cat > "${TMP_PATH}/package/Component.plist" << EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
    <dict>
        <key>BundleHasStrictIdentifier</key>
        <false/>
        <key>BundleIsRelocatable</key>
        <false/>
        <key>BundleIsVersionChecked</key>
        <false/>
        <key>BundleOverwriteAction</key>
        <string>upgrade</string>
        <key>RootRelativeBundlePath</key>
        <string>Applications/${APP_NAME}.app</string>
    </dict>
</array>
</plist>
EOF

}

# Builds a PackageInfo file (0 arguments)
buildPackageInfo() {

	# Get additional information
	local FILE_COUNT=$(find "${TMP_PATH}/root" | wc -l)
	local INSTALL_SIZE=$(du -k -s "${TMP_PATH}/root" | awk '{print $1}')

# Write file
cat > "${TMP_PATH}/package/${APP_NAME}.pkg/PackageInfo" << EOF
<?xml version="1.0" encoding="utf-8"?>
<pkg-info overwrite-permissions="true" relocatable="false" identifier="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component" postinstall-action="none" version="${APP_VERSION}" format-version="2" auth="root">
    <payload numberOfFiles="${FILE_COUNT}" installKBytes="${INSTALL_SIZE}"/>
    <bundle path="./Applications/${APP_NAME}.app" id="${APP_IDENTIFIER}" CFBundleShortVersionString="${APP_VERSION}" CFBundleVersion="${APP_VERSION_BUILD}"/>
    <bundle-version/>
    <upgrade-bundle>
        <bundle id="${APP_IDENTIFIER}"/>
    </upgrade-bundle>
    <update-bundle/>
    <atomic-update-bundle/>
    <strict-identifier/>
    <relocate/>
</pkg-info>
EOF

}

# Builds a Distribution file (0 arguments)
buildDistribution() {

	# Background
	if [[ -f "${RESOURCES_PATH}/background.png" ]]; then
		local BACKGROUND_LIGHT='<background file="background.png" mime-type="image/png" alignment="bottomleft" scaling= "proportional"/>'
		local BACKGROUND_DARK='<background-darkAqua file="background.png" mime-type="image/png" alignment="bottomleft" scaling= "proportional"/>'
	elif [[ -f "${RESOURCES_PATH}/background-light.png" && -f "${RESOURCES_PATH}/background-dark.png" ]]; then
		local BACKGROUND_LIGHT='<background file="background-light.png" mime-type="image/png" alignment="bottomleft" scaling= "proportional"/>'
		local BACKGROUND_DARK='<background-darkAqua file="background-dark.png" mime-type="image/png" alignment="bottomleft" scaling= "proportional"/>'
	fi

	# Welcome
	if [[ -f "${RESOURCES_PATH}/welcome.rtf" ]]; then
		local WELCOME='<welcome file="welcome.rtf" mime-type="text/rtf"/>'
	elif [[ -f "${RESOURCES_PATH}/welcome.txt" ]]; then
		local WELCOME='<welcome file="welcome.txt" mime-type="text/plain"/>'
	fi

	# Read me
	if [[ -f "${RESOURCES_PATH}/readme.rtf" ]]; then
		local README='<readme file="readme.rtf" mime-type="text/rtf"/>'
	elif [[ -f "${RESOURCES_PATH}/readme.txt" ]]; then
		local README='<readme file="readme.txt" mime-type="text/plain"/>'
	fi

	# License
	if [[ -f "${RESOURCES_PATH}/license.rtf" ]]; then
		local LICENSE='<license file="license.rtf" mime-type="text/rtf"/>'
	elif [[ -f "${RESOURCES_PATH}/license.txt" ]]; then
		local LICENSE='<license file="license.txt" mime-type="text/plain"/>'
	fi

	# Conclusion
	if [[ -f "${RESOURCES_PATH}/conclusion.rtf" ]]; then
		local CONCLUSION='<conclusion file="conclusion.rtf" mime-type="text/rtf"/>'
	elif [[ -f "${RESOURCES_PATH}/conclusion.txt" ]]; then
		local CONCLUSION='<conclusion file="conclusion.txt" mime-type="text/plain"/>'
	fi

	# Encode app name
	urlEncode "$APP_NAME"
	if [[ $OS_KERNEL == "Linux" ]]; then
		local APP_NAME_ENCODED="#${URL_ENCODE_RESULT}"
	else
		local APP_NAME_ENCODED="${URL_ENCODE_RESULT}"
	fi

# Write file
cat > "${TMP_PATH}/package/Distribution" << EOF
<?xml version="1.0" encoding="utf-8"?>
<installer-gui-script minSpecVersion="1">
    <title>${APP_NAME}</title>
    ${BACKGROUND_LIGHT}
    ${BACKGROUND_DARK}
    ${WELCOME}
    ${README}
    ${LICENSE}
    ${CONCLUSION}
    <pkg-ref id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component"/>
    <options customize="never" require-scripts="false" hostArchitectures="x86_64,arm64"/>
    <choices-outline>
        <line choice="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component"/>
    </choices-outline>
    <choice id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component" title="${APP_NAME}">
        <pkg-ref id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component"/>
    </choice>
    <pkg-ref id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component">
        <must-close>
            <app id="${APP_IDENTIFIER}"/>
        </must-close>
        <bundle-version>
            <bundle CFBundleShortVersionString="${APP_VERSION}" CFBundleVersion="${APP_VERSION_BUILD}" id="${APP_IDENTIFIER}" path="Applications/${APP_NAME}.app"/>
        </bundle-version>
    </pkg-ref>
    <pkg-ref id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.component" version="${APP_VERSION}" onConclusion="none">${APP_NAME_ENCODED}.pkg</pkg-ref>
    <product id="${APP_IDENTIFIER}.${APP_NAME_FLAT}.pkg.product" version="${APP_VERSION}"/>
</installer-gui-script>
EOF

}

################################################################################
# Console output                                                               #
################################################################################

# Prints an info message (1..* arguments)
printInfo() {

	if [[ $# == 1 ]]; then
		echo -e "${BOLD_GREEN}$1${NORMAL}"
	elif [[ $# -gt 1 ]]; then
		local HEADLINE=$1
		shift
		echo -e "${BOLD_GREEN}${HEADLINE}:${NORMAL_GREEN} $@${NORMAL}"
	fi

}

# Prints a warning message (0..* arguments)
printWarning() {

	if [[ $# == 0 ]]; then
		echo -e "${BOLD_YELLOW}Warning:${NORMAL_YELLOW} A warning occurred.${NORMAL}"
	else
		echo -e "${BOLD_YELLOW}Warning:${NORMAL_YELLOW} $@${NORMAL}"
	fi

}

# Prints an error message and exits (0..* arguments)
printError() {

	if [[ $# == 0 ]]; then
		echo -e "${BOLD_RED}Error:${NORMAL_RED} An error occurred.${NORMAL}"
	else
		echo -e "${BOLD_RED}Error:${NORMAL_RED} $@${NORMAL}"
	fi
	exit 1

}

# Prints usage information and exits (0 arguments)
printUsage() {

	echo -e "${BOLD}Usage:${NORMAL}${USAGE}"
	exit 0

}

################################################################################
# Run the actual script                                                        #
################################################################################

build "$@"
